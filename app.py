from threading import Lock
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, disconnect
import time
import random
import os
import serial
import string

global globSpeed
globSpeed = 1500
global globAngle
globAngle = 1500
global dbErr
dbErr = 0
global laErr
laErr = 0
global laON
laON = 0
global atON
atON = 0
global led2
led2 = -1
global led3
led3= -1
global led4
led4 = -1
global led17
led17 = -1
global distance
distance = 3000

#skusi inicializovat GPIO piny lediek
try:
    import RPi.GPIO as IO
    IO.setwarnings(False)
    IO.setmode (IO.BCM)
    IO.setup(2,IO.IN) # Prava bocna
    IO.setup(3,IO.IN) # Prava stredna
    IO.setup(4,IO.IN) # Lava Stredna
    IO.setup(17,IO.IN) # Lava Bocna
except:
    print("Failed to Import GPIO library!")
    laErr=1

global usbAUTO
usbAUTO='USB0'
global usbARDUINO
usbARDUINO='USB1'

async_mode = None

app = Flask(__name__)

#skusi rozbehat databazu
try:
    import MySQLdb
    import ConfigParser
    config = ConfigParser.ConfigParser()
    config.read('config.cfg')
    myhost = config.get('mysqlDB', 'host')
    myuser = config.get('mysqlDB', 'user')
    mypasswd = config.get('mysqlDB', 'passwd')
    mydb = config.get('mysqlDB', 'db')
    db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
    cursor = db.cursor()
except:
    print("Nepodarilo sa nadviazat spojenie s DB!")
    dbErr=1



app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()


def background_thread(args):
    global dbErr
    global atErr
    global laErr
    global laON
    global atON
    global globSpeed
    global globAngle
    global led2
    global led3
    global led4
    global led17
    global serialAuto
    global serialArduino
    global distance

    while True:
        if laON ==1:       #Lane Assist
            try:
                if IO.input(3)==0: # zatoc trochu doprava
                    globAngle=1650
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")
                if IO.input(3)==0 and IO.input(2)==0: #zatoc viac doprava
                    globAngle=1800
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")
                if IO.input(4)==0: # zatoc trochu dolava
                    globAngle=1350
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")
                if IO.input(4)==0 and IO.input(17)==0: #zatoc viac dolava
                    globAngle=1200
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")

                #Ulozenie pre DB a frontend
                led2=IO.input(2)
                led3=IO.input(3)
                led4=IO.input(4)
                led17=IO.input(17)
            except:
                print("Lane Assist Error!")
                laErr=1
                led2=-1
                led3=-1
                led4=-1
                led17=-1

        if atON ==1: # Adaptivny Tempomat
            try:
                riadok = serialArduino.readline().decode('utf-8')
                cislo=16
                tmp=riadok[cislo]+riadok[cislo+1]+riadok[cislo+2]+riadok[cislo+3]
                print(tmp)
                distance=int(tmp)
                if distance<1500 and distance>500:
                    globSpeed=globSpeed-10
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")
                if distance<500:
                    globSpeed=1500
                    serialAuto.write(str(globAngle)+"s"+str(globSpeed)+"t")
            except:
                print("Adaptivny Tempomat Error!")
                atErr=1

        socketio.sleep(2)
        socketio.emit('backend_response',
                      {'data': 'datastream',
                       'rychlost': globSpeed,
                       'uhol': globAngle,
                       'objectDistance': distance,
                       'ledRightSide': led2,
                       'ledRightMiddle': led3,
                       'ledLeftMiddle': led4,
                       'ledLeftSide': led17,
                      },
                      namespace='/test')
        if dbErr==0:
            try:
                dataDict = {
                   'rychlost': globSpeed,
                   'uhol': globAngle,
                   'objectDistance': distance,
                   'ledRightSide': led2,
                   'ledRightMiddle': led3,
                   'ledLeftMiddle': led4,
                   'ledLeftSide': led17}
                cursor.execute("SELECT MAX(id) FROM prva")
                maxid=cursor.fetchone()
                cursor.execute("INSERT INTO prva (id,popis) VALUES (%s, %s)",(maxid[0]+1, str(dataDict).replace("'","\"")))
                db.commit()
            except:
                print("Neznama chyba DB!")
                dbErr=1

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@socketio.on('button_event', namespace='/test')
def buttonHandler(message):
    global laON
    global atON
    global usbAUTO
    global globSpeed
    global globAngle
    if message['value']=="Start":
        print("Start prompt received")
        try:
            global serialAuto
            serialAuto = serial.Serial(
                    port=('/dev/tty'+usbAUTO),
                    baudrate=115200,
                    timeout=1
                    )
            serialAuto.write("1100s1500t")
            time.sleep(1)
            serialAuto.write("1900s1500t")
            time.sleep(1)
            serialAuto.write("1500s1500t")
            globSpeed=1500
            globAngle=1500
        except:
            print("Failed to initialize serial connection!")
            autoErr = 1
            socketio.emit('backend_response',
                            {'data': "startError",
                            'dbStav': dbErr},
                            namespace='/test')


    if message['value']=="Stop":
        print("Stop signal received")
        try:
            serialAuto.write("1500s1500t")
            globSpeed=1500
            globAngle=1500
            laON = 0
            atON = 0
            disconnect()
        except:
            print("Tlacidlo STOP zlyhalo!")


@socketio.on('checkbox_event', namespace='/test')
def checkboxHandler(message):
    global laON
    global atON
    global laErr
    global usbARDUINO
    global serialArduino

    if message['value']=="startLaneAssist":
        print("Starting LA")
        laON = 1
        if laErr==1:
            socketio.emit('backend_response',
                            {'data': "laError"},
                            namespace='/test')
    if message['value']=="stopLaneAssist":
        print("Stopping LA")
        laON = 0


    if message['value']=="startAdaptiveTempomat":
        print("Starting AT")
        try:
            serialArduino = serial.Serial(('/dev/'+usbARDUINO),9600)
            atON = 1
        except:
            print("AT error!")
            atON = 0
            socketio.emit('backend_response',
                            {'data': "atError"},
                            namespace='/test')


    if message['value']=="stopAdaptiveTempomat":
        print("Stopping AT")
        atON = 0

    if message['value']=="startWebcam":
        print("Starting Webcam")
        os.system("echo raspberry | sudo -S service motion start")

    if message['value']=='stopWebcam':
        print("Stopping Webcam")
        os.system("echo raspberry | sudo -S service motion stop")


@socketio.on('slider_event', namespace='/test')
def sliderHandler(message):
    global serialAuto
    global globSpeed
    global globAngle
    if message['type']=="Rychlost":
        print("Slider Rychlosti ", message['value'])
        globSpeed = 1500+int(int(message['value'])*80/100)
        fullCommand=str(globAngle)+"s"+str(globSpeed)+"t"
        try:
            serialAuto.write(fullCommand)
        except:
            print("Serial write failed!")
            socketio.emit('backend_response',
                            {'data': "writeError"
                            },
                            namespace='/test')

    elif message['type']=="Uhol":
        print("Slider uhla", message['value'])
        globAngle = 1500+int(message['value'])
        fullCommand=str(globAngle)+"s"+str(globSpeed)+"t"
        try:
            serialAuto.write(fullCommand)
        except:
            print("Serial write failed!")
            socketio.emit('backend_response',
                            {'data': "writeError"
                            },
                            namespace='/test')


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread, args=session._get_current_object())
#    emit('backend_response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)



if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
