var sliderSpeed = document.getElementById("autoRychlost");
var sliderAngle = document.getElementById("autoUhol");
var speed = sliderSpeed.value;
var g1 = new JustGage({
  id: "gaugeSpeed",
  value: speed,
  min: 0,
  max: 100,
  title: "Rychlost"
});
var g2 = new JustGage({
  id: "gaugeAngle",
  value: sliderAngle.value,
  min: -400,
  max: 400,
  pointer: true,
  pointerOptions: {
    toplength: 10,
    bottomlength: 20,
    bottomwidth: 12,
    color: '#000',
  },
  gaugeColor: "#fff",
  levelColors: ["#fff"],
  title: "Natocenie Kolies"
});
sliderSpeed.oninput = function() {
  g1.refresh(this.value);
}
sliderAngle.oninput = function() {
  g2.refresh(this.value);
}
