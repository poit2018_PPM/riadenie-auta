function startEvent() {
  var startButton = document.getElementById("btnStart");
  var stopButton= document.getElementById("btnStop");

  startButton.innerHTML = "Running";
  startButton.style.backgroundColor="";
  stopButton.style.backgroundColor="rgb(190, 50, 50)";
  stopButton.innerHTML="Stop";

}

function stopEvent() {
  var startButton = document.getElementById("btnStart");
  var stopButton= document.getElementById("btnStop");

  stopButton.innerHTML = "Stopped";
  startButton.style.backgroundColor="rgb(50, 190, 50)";
  stopButton.style.backgroundColor="";
  startButton.innerHTML="Start";
}
