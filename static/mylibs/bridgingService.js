$(document).ready(function() {
  namespace = '/test';
  var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
  var x = new Array();
  var y = new Array();
  var count = 0;
  socket.on('connect', function() {
    socket.emit('my_event', {
      data: 'I\'m connected!',
      value: 1
    });
  });


  socket.on('backend_response', function(msg) {
    //console.log(msg.rychlost);
    g1.refresh((msg.rychlost-1500)/80*100);
    g2.refresh(msg.uhol-1500);
    document.getElementById('dist').innerHTML=msg.objectDistance;

    if (msg.data=="startError"){
      document.getElementById("TestingHeader").style.display="block";
      console.error("ERR: Nepodarilo sa pripojit k Autu!")
    }
    if (msg.data=="writeError"){
      console.error("ERR: Seriova komunikacia zlyhala!")
    }

    if (msg.data=="laError"){
      console.error("ERR: Lane Assist zlyhal!")
      document.getElementById("laStav").innerHTML="ERROR";
      document.getElementById("laStav").style.backgroundColor="rgb(255, 0, 0)";
    }

    if (msg.data=="atError"){
      console.error("ERR: Lane Assist zlyhal!")
      document.getElementById("atStav").innerHTML="ERROR";
      document.getElementById("atStav").style.backgroundColor="rgb(255, 0, 0)";
    }

    if(msg.dbStav){
      console.error("ERR: Nepodarilo sa pripojit k MySQL databaze!")
    }
    if (msg.data=="datastream"){
      count = count + 1;
      x.push((count));
      y.push((msg.rychlost));
      trace = {
          x: x,
          y: y,
      };
      layout = {
        title: 'Data',
        xaxis: {
            title: 'x',
        },
        yaxis: {
            title: 'y',
            //range: [-1,1]
        }
      };
      var traces = new Array();
      traces.push(trace);
      Plotly.newPlot('graf1', traces, layout);


      if(msg.ledRightSide==1){
        document.getElementById("rightSide").style.backgroundColor="rgb(0, 255, 0)";
      }
      else if(msg.ledRightSide==0){
        document.getElementById("rightSide").style.backgroundColor="rgb(255, 0, 0)";
      }
      if(msg.ledRightSide==-1){
        document.getElementById("rightSide").style.backgroundColor="rgb(0, 0, 0)";
      }

      if(msg.ledRightMiddle==1){
        document.getElementById("rightMiddle").style.backgroundColor="rgb(0, 255, 0)";
      }
      else if(msg.ledRightMiddle==0){
        document.getElementById("rightMiddle").style.backgroundColor="rgb(255, 0, 0)";
      }
      if(msg.ledRightMiddle==-1){
        document.getElementById("rightMiddle").style.backgroundColor="rgb(0, 0, 0)";
      }

      if(msg.ledLeftSide==1){
        document.getElementById("leftSide").style.backgroundColor="rgb(0, 255, 0)";
      }
      else if(msg.ledLeftSide==0){
        document.getElementById("leftSide").style.backgroundColor="rgb(255, 0, 0)";
      }
      if(msg.ledLeftSide==-1){
        document.getElementById("leftSide").style.backgroundColor="rgb(0, 0, 0)";
      }

      if(msg.ledLeftMiddle==1){
        document.getElementById("leftMiddle").style.backgroundColor="rgb(0, 255, 0)";
      }
      else if(msg.ledLeftMiddle==0){
        document.getElementById("leftMiddle").style.backgroundColor="rgb(255, 0, 0)";
      }
      if(msg.ledLeftMiddle==-1){
        document.getElementById("leftMiddle").style.backgroundColor="rgb(0, 0, 0)";
      }

    }
     //$('#log').append('Received #'+msg.count+': '+msg.data+'<br>').html();
  });

  $('#btnStart').click(function() {
    socket.emit('button_event', {
      value: "Start"
    });
    console.log("Started")
    return false;
  });
  $('#btnStop').click(function() {
    socket.emit('button_event', {
      value: "Stop"
    });
    console.log("Stopped")
    return false;
  });

  $('#la').change(function() {
    if (this.checked) {
      socket.emit('checkbox_event', {
        value: "startLaneAssist"
      });
      console.log("Starting LA");
    }
    else if(!(this.checked)){
      socket.emit('checkbox_event', {
                    value: "stopLaneAssist"
                  });
      console.log("Stopping LA");
    }
    return false;
  });
  $('#at').change(function() {
    if (this.checked) {
      socket.emit('checkbox_event', {
        value: "startAdaptiveTempomat"
      });
      console.log("Starting AT");
    }
    else if(!(this.checked)){
      socket.emit('checkbox_event', {
                    value: "stopAdaptiveTempomat"
                  });
      console.log("Stopping AT");
    }
    return false;
  });


  $('#cam').change(function() {
    if (this.checked) {
      socket.emit('checkbox_event', {
        value: "startWebcam"
      });
      console.log("Starting Webcam");
    }
    else if(!(this.checked)){
      socket.emit('checkbox_event', {
                    value: "stopWebcam"
                  });
      console.log("Stopping Webcam");
    }
    return false;
  });

  $('#autoRychlost').change(function() {
    socket.emit('slider_event', {
      type: "Rychlost",
      value: this.value
    });
    console.log("Speed Slider Worked");
    return false;
  });

  $('#autoUhol').change(function() {
    socket.emit('slider_event', {
      type: "Uhol",
      value: this.value
    });
    console.log("Angle Slider Worked");
    return false;
  });
});
